package com.example.sortingapp;

import org.junit.Test;

import java.util.Arrays;

public class SortingAppTest {

    @Test(expected = IllegalArgumentException.class)
    public void testThrowException_whenTooManyArguments() {
        int[] intArgs = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        String[] args = Arrays.stream(intArgs).mapToObj(String::valueOf).toArray(String[]::new);
        App.main(args);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThrowException_whenNotNumberArguments() {
        String[] args = {"abc", "def"};
        App.main(args);
    }
}
