package com.example.sortingapp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        if (args.length == 0) {
            logger.error("No arguments provided.");
            return;
        }
        if (args.length > 10) {
            throw new IllegalArgumentException("the number of elements is more then 10");
        }


        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Not a number");
            }
        }

        Arrays.sort(numbers);

        logger.info("Sorted numbers:");
        for (int number : numbers) {
            logger.info(number);
        }
    }
}